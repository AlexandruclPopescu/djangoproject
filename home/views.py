from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse('Hello, Alex')

# @login_required
def home(request):
    context = {
        "all_students": [
            {
                'first_name': 'Alex',
                'last_name': 'Popescu',
                'age': 42
            },
            {
                'first_name': 'Mircea',
                'last_name': 'Popovici',
                'age': 32
            }

        ]
    }
    return render(request, 'home/home.html', context)

def brands(request):
    context = {
        "masini": [
            {
                'marca': 'Alfa Romeo',
                'motorizare': 'Diesel',
                'cilindree': 2000,
                'culoare': 'Rosu'
            },
            {
                'marca': 'Audi',
                'motorizare': 'Diesel',
                'cilindree': 2000,
                'culoare': 'Albastru'

            },
            {
                'marca': 'Porche',
                'motorizare': 'Benzina',
                'cilindree': 2000,
                'culoare': 'Galben'

            }
        ]
    }
    return render(request, 'home/masini.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html' # afisam o pagina html