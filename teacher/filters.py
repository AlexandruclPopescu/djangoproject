import django_filters

from teacher.models import Teacher


class TeacherFilters(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'specialisation', 'date_of_birth', 'created_at']

