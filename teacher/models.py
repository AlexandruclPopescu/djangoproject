from django.db import models


class Teacher(models.Model):
    gender_choices = (
        ('Male', 'Male'),
        ('Female', 'Female')
    )

    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    specialisation = models.CharField(max_length=30, null=True)
    date_of_birth = models.DateField(null=True)
    active = models.BooleanField(default=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} - {self.specialisation}'
