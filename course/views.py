from django.shortcuts import render

# Create your views here.
from teacher.models import Teacher


def get_teachers_per_course(request, teacher_id):
    all_teacher_per_course = Teacher.objects.filter(id=teacher_id)
    context = {'get_all_teachers': all_teacher_per_course}
    return render(request, 'courses/get_all_teacher_per_course.html', context)
