from django.urls import path

from course import views

urlpatterns = [
    path('teachers_per_courses/<int:teacher_id>/', views.get_teachers_per_course, name='teacher-per-coursers')
]
# 'teachers_per_courses' este ce apare in browser, views.get este metoda
