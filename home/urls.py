from django.urls import path
from home import views

# constructia unui url
urlpatterns = [
    path('page/', views.homepage, name='page'),
    path('all_students/', views.home, name='list_of_students'),
    path('masini/', views.brands, name='masini'),
    path('', views.HomeTemplateView.as_view(), name='homepage')

]