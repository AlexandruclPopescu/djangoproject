# from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from student.models import Student
from teacher.filters import TeacherFilters
from teacher.forms import TeacherForm
from teacher.models import Teacher


# mai jos creem formularul
class TeacherCreateView(LoginRequiredMixin, CreateView):
    template_name = 'teacher/create_teacher.html'  # calea catre fisierul html unde va fi afisat formularul
    model = Teacher                                 # modelul=tabelul pe care il folosim in generarea formuarului
    # fields = '__all__'                            # campurile pe care le vrem in formular
    success_url = reverse_lazy('create-teacher')  # redirectionarea dupa ce formularul a fost salvat
    form_class = TeacherForm


class TeacherListView(LoginRequiredMixin, ListView):
    template_name = 'teacher/list_teacher.html'
    model = Teacher
    context_object_name = 'all_teachers'

    def get_context_data(self, **kwargs):
        data = super(TeacherListView, self).get_context_data(**kwargs)
        all_teachers = Teacher.objects.all()
        my_filter = TeacherFilters(self.request.GET, queryset=all_teachers)
        all_teachers = my_filter.qs
        data['all_teachers'] = all_teachers
        data['my_filter'] = my_filter
        return data


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'teacher/update_teacher.html'
    model = Teacher
    success_url = reverse_lazy('list-of-teachers')
    form_class = TeacherForm


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'teacher/delete_teacher.html'
    model = Teacher
    success_url = reverse_lazy('list-of-teachers')


class TeacherDetailView(LoginRequiredMixin, DetailView):
    template_name = 'teacher/detail_teacher.html'
    model = Teacher


def get_all_students_per_teacher(request, id_teacher):
    all_students_per_teacher = Student.objects.filter(teacher_id=id_teacher)
    context = {'students': all_students_per_teacher}
    return render(request, 'student/get_all_students_per_teacher.html', context)



