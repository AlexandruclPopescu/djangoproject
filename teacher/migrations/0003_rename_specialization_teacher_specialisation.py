# Generated by Django 3.2.9 on 2021-11-18 18:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0002_auto_20211118_1816'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='specialization',
            new_name='specialisation',
        ),
    ]
